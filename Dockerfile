FROM fluendo/docker-uwsgi-nginx:latest

# To avoid any interactive question
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get install -y libldap2-dev libsasl2-dev \
    postgresql-common libpq-dev libnss-ldap git sudo && \
    rm -rf /var/lib/apt/lists/*
ADD requirements.txt /app/
WORKDIR /app
RUN pip install -r requirements.txt

# Copy the configuration
COPY etc/ /etc/

EXPOSE 80

COPY run.sh .
CMD ["./run.sh"]
