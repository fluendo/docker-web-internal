#!/bin/bash

# Only run this script as buildbot user
# we do this here instead of the Dockerfile to give time
# on the system to mount NSS new configuration from the
# host
user=`whoami`
if [ -z $user ]; then
    echo "Something is wrong with the LDAP/NSS"
    exit 1
fi

if [ "$user" = "root" ]; then
    echo "Running this command as buildbot user"
    su -c ./run.sh buildbot
    if [ $? -ne 0 ]; then
      echo "Can't launch the command as user buildbot"
      exit 1
    fi
    echo "Starting up the supervisor"
    # Launch uwsgi/nginx
    supervisord
    exit 0
fi

if [ "$user" != "buildbot" ]; then
    echo "This command must be run as root"
    exit 1
fi

# Check envvars
if [ -z ${PROJECT_URL} ]; then
    echo "You must specify the PROJECT_URL envvar"
    exit 1
fi

# create the home in case it does not exist
if [ ! -d $HOME ]; then
    sudo mkdir -p $HOME
fi

owner="$(stat --format '%U' "$HOME")"
if [ "$owner" != "buildbot" ]; then
    sudo chown buildbot $HOME
fi

cd $HOME
# Clone the web internal project
if [ ! -e project ]; then
    git clone $PROJECT_URL project
fi
